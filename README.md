Serva_ arXiv_2011.08825_2020  
 
Contains input files and data used to generate the figures of the article:  
 
Size-dependence of hydrophobic hydration at electrified gold/water interfaces 
 
Alessandra Serva, Mathieu Salanne, Martina Havenith and Simone Pezzotti, *arXiv*, 2011.08825, 2020
 
[https://arxiv.org/abs/2011.08825]( https://arxiv.org/abs/2011.08825)
 
The folder *typical_input_files* contains typical MetalWalls input files used to perform the simulations. 
 
The folder *DATA_ASCII_FIGURES* contains the processed data used to plot all the figures of the paper.
